package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
     int rows;
     int cols;
     CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        cellState = new CellState[rows][columns];
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                cellState[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException {
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {
            cellState[row] [column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException {
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {
            return cellState[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    } 

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                newGrid.cellState[row][col] = this.cellState[row][col];
            }
        }
        return newGrid;   
    }
}
            
    

