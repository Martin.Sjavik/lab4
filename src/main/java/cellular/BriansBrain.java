package cellular;

import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{


    IGrid currentGeneration;


    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				int rand = random.nextInt(3);
				if (rand == 0) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} 
				else if (rand == 1){
					currentGeneration.set(row, col, CellState.DEAD);
				}
				else {
					currentGeneration.set(row, col, CellState.DYING);
				}
			}
		}
	}

    @Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();

        for (int row = 0; row < numberOfRows()-1; row++) {
            for (int col = 0; col < numberOfColumns()-1; col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

    @Override
	public CellState getNextCell(int row, int col) {
		CellState curr = currentGeneration.get(row, col);
		int check = countNeighbors(row, col, CellState.ALIVE);
		if (curr.equals(CellState.ALIVE)) {
			return CellState.DYING;
		}
		else if (curr.equals(CellState.DYING)) {
			return CellState.DEAD;
		}
		else if (curr.equals(CellState.DEAD) && check == 2) {
			return CellState.ALIVE;
		}
		else {
			return curr;
		}
	}

    private int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;

		for (int i =row-1; i <= row+1; i++) {
			for (int j = col-1; j <= col+1; j++) {
				if ((i == row && j == col) || i < 0 || i > numberOfRows() || j > numberOfColumns() || j < 0 || state != getCellState(i, j)) {
					continue;
				}
				else {
					neighbors++;
				}
			}	
		}
		return neighbors;
	}

    @Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

    @Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
    
}
